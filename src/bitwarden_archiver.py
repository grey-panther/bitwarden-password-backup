import argparse
import datetime
import getpass
import json
import logging
import os
import re
import shutil
import subprocess
import sys
import tempfile

import password_strength  # type: ignore
import py7zr

parser = argparse.ArgumentParser(description="Archive bitwarden contents")
parser.add_argument(
    "-bw",
    "--bitwarden-cli",
    dest="bitwarden_cli",
    default="bw",
    help="Path to bitwarden command line client",
)
parser.add_argument(
    "-u",
    "--use-existing-session",
    dest="use_session",
    action="store_true",
    help=(
        "Use the existing logged in Bitwardend CLI session instead of logging in (this only backs"
        " up the one logged in account)"
    ),
)
parser.add_argument(
    "-l",
    "--user",
    dest="user_logins",
    action="append",
    help=(
        "Specify the login of the user(s) to be backed up. This can be repeated for backing up"
        " multiple accounts."
    ),
)
parser.add_argument(
    "-o",
    "--output-file",
    dest="output",
    required=True,
    help="Output archive to save the backup into",
)
parser.add_argument(
    "-v",
    "--verbose",
    dest="verbose",
    action="store_true",
    default=False,
    help=(
        "Verbose debugging. Warning! Debug logs contain detailed information that could be a"
        " security risk in unwanted persons get access to it!"
    ),
)


def _bw(args, parse_output=True):
    try:
        logging_args = list(args)
        try:
            i = logging_args.index("login")
            for i in range(i + 1, len(logging_args)):
                logging_args[i] = "..."
        except ValueError:
            pass
        logging.debug("Running %s", logging_args)
        output = subprocess.check_output(args)
    except Exception as e:
        if isinstance(e, subprocess.CalledProcessError):
            logging.error("Failed to run %s. Exited with code %d", logging_args, e.returncode)
        else:
            logging.error("Failed to run %s. %s", logging_args, e)
        raise e
    output = output.decode("utf-8")
    if parse_output:
        output = json.loads(output)
    return output


def _login(bw_cli, username=None):
    while True:
        if username:
            input_username = username
            password = getpass.getpass("Password for %s (ctrl+c to abort): " % username)
        else:
            try:
                input_username = input("Username (empty to finish, ctrl+c to abort): ")
            except EOFError:
                return ""
            if not input_username:
                return ""
            password = getpass.getpass("Password: ")
        logging.info("Logging in to Bitwarden")
        try:
            output = _bw(bw_cli + ["login", input_username, password], parse_output=False)
        except subprocess.CalledProcessError:
            logging.error("Error during login, retry")
            continue
        m = re.search(r'export BW_SESSION="(.*)"', output)
        return m.group(1)


_tempfile = None


def _get_temp_file():
    global _tempfile
    if _tempfile:
        return _tempfile
    tmp = tempfile.NamedTemporaryFile(delete=False)
    tmp.close()
    _tempfile = tmp.name
    return _tempfile


def _erase_temp_file():
    global _tempfile
    if not _tempfile:
        return
    logging.debug("Overwriting %s with random data", _tempfile)
    passes = 10
    with open(_tempfile, "r+b", buffering=0) as f:
        size = f.seek(0, os.SEEK_END)
        for i in range(passes):
            logging.debug("Pass %d out of %d", i + 1, passes)
            f.seek(0)
            f.write(os.urandom(size))
    logging.debug("Overwrite done")
    logging.debug("Truncating %s", _tempfile)
    with open(_tempfile, "w"):
        pass
    logging.debug("Truncated")


def _export(bw_cli, session_var, org=None):
    logging.info("Exporting")
    tmp = _get_temp_file()
    logging.debug("Exporting organization %s to %s", org, tmp)
    try:
        command = bw_cli + ["export", "--format", "json", "--output", tmp, "--session", session_var]
        if org:
            command += ["--organizationid", org]
        _bw(command, parse_output=False)
        with open(tmp) as f:
            return json.load(f)
    finally:
        # The bitwarden cli doesn't offer a way to stream the export directly to stdout,
        # just to a specific file. So do the next best thing: try to securely erase the contents
        # of the file afterwards and hope that no other process has captured it :(.
        _erase_temp_file()


def _read_backup_password():
    while True:
        password1 = getpass.getpass("Password for backup archive: ")
        password2 = getpass.getpass("Repeat: ")
        if password1 != password2:
            logging.error("The two passwords didn't match, please retry!")
            continue
        test = password_strength.PasswordPolicy().password(password1)
        if test.strength() < 0.66:
            while True:
                answer = input(
                    "This seems like a weak password (%.2f out of 1.0). Are you sure you want to"
                    " use it? [y/n]"
                    % test.strength()
                ).lower()
                if answer in ["y", "n"]:
                    break
                logging.error("Please answer y or n")
            if answer == "n":
                continue
        return password1


def _read_archive(fn, password):
    logging.debug("Reading %s", fn)
    with py7zr.SevenZipFile(fn, password=password) as f:
        result = {name: bf.read() for name, bf in f.readall().items()}
    logging.debug("Read %d entries", len(result))
    return result


def run(args) -> int:
    try:
        bw_cli = [args.bitwarden_cli, "--nointeraction"]
        logging.info("Checking current bitwarden status")
        status = _bw(bw_cli + ["status"])
        expected_status = "unlocked" if args.use_session else "unauthenticated"
        if status["status"] != expected_status:
            logging.error(
                "Bitwarded client status is %s (expected %s)", status["status"], expected_status
            )
            return 1

        def _export_all():
            result = {}
            args_login_index = 0
            while True:
                if args.use_session:
                    if args.user_logins:
                        logging.error("Can't specify -u and -l at the same time")
                        return 1
                    session_var = os.environ["BW_SESSION"]
                else:
                    if args.user_logins:
                        if args_login_index >= len(args.user_logins):
                            break
                        session_var = _login(bw_cli, username=args.user_logins[args_login_index])
                        args_login_index += 1
                    else:
                        session_var = _login(bw_cli)
                    if session_var == "":
                        break
                    if session_var is None:
                        logging.error("Error during login, aborting")
                        return 1

                status = _bw(bw_cli + ["status"])
                user_id = status["userId"]
                result[user_id] = {"": _export(bw_cli, session_var)}
                for org in _bw(bw_cli + ["list", "organizations", "--session", session_var]):
                    if org["type"] not in {0, 1}:
                        # only owners and admins can export organizations:
                        # https://bitwarden.com/help/export-your-data/#export-an-organization-vault
                        # constant mapping:
                        # https://github.com/bitwarden/server/blob/7f5f010e1eea400300c47f776604ecf46c4b4f2d/src/Core/Enums/OrganizationUserType.cs
                        continue
                    result[user_id][org["id"]] = _export(bw_cli, session_var, org=org["id"])

                if args.use_session:
                    break
                else:
                    logging.info("Logging out from Bitwarden")
                    _bw(bw_cli + ["logout"], parse_output=False)
            return result

        export = json.dumps(_export_all(), indent=2, sort_keys=True).encode("ascii")

        if os.path.exists(args.output):
            backup_password = getpass.getpass("Archive password: ")
            logging.info("Reading exiting archive")
            existing_archive = _read_archive(args.output, backup_password)
        else:
            backup_password = _read_backup_password()
            existing_archive = {}

        if export == existing_archive.get("backup.json"):
            logging.info("Nothing changed")
            return 0

        backup_backup_name = datetime.datetime.now().strftime("backup_backups/%Y/%m-%d/backup.json")
        logging.debug("Backup name for backup.json: %s", backup_backup_name)
        if backup_backup_name in existing_archive:
            i = 1
            while ("%s.%d" % (backup_backup_name, i)) in existing_archive:
                i += 1
            backup_backup_name = "%s.%d" % (backup_backup_name, i)
            logging.debug("Backup name already exists, trying %s", backup_backup_name)

        logging.info("Creating temporary new archive")
        tmp_out = _get_temp_file()
        logging.debug("Writing temporary archive to %s", tmp_out)
        with py7zr.SevenZipFile(tmp_out, mode="w", password=backup_password) as f:
            f.set_encrypted_header(True)
            logging.debug("Writing %s", "backup.json")
            f.writestr(export, "backup.json")
            if "backup.json" in existing_archive:
                logging.debug("Backing up backup.json to %s", backup_backup_name)
                f.writestr(existing_archive["backup.json"], backup_backup_name)
            for name, content in existing_archive.items():
                if name == "backup.json":
                    continue
                logging.debug("Copying %s", name)
                f.writestr(content, name)

        logging.info("Checking new temporary archive")
        with py7zr.SevenZipFile(tmp_out, password=backup_password) as f:
            ok = f.test()
        if not ok:
            logging.error("New archive failed CRC check")
            os.unlink(tmp_out)
            return 1

        new_archive = _read_archive(tmp_out, backup_password)
        logging.debug("Checking that copied over content still matches")
        for name, content in existing_archive.items():
            if name == "backup.json":
                continue
            logging.debug("Checking %s", name)
            if content != new_archive.get(name):
                logging.error("Content mismatch for new archive!")
                os.unlink(tmp_out)
                return 1

        logging.info("Copying temporary archive to the final destination")
        try:
            shutil.copyfile(tmp_out, args.output)
        except Exception as e:
            logging.error(
                (
                    "There was an error during the copy! The temporary archive is still available"
                    " at %s"
                ),
                tmp_out,
            )
            raise e

        # For extra paranoia - even though it should be encrypted
        _erase_temp_file()
    except Exception as e:
        logging.error("Error %s", e)
        return 1
    return 0


if __name__ == "__main__":
    args = parser.parse_args()
    logging.basicConfig(level=logging.DEBUG if args.verbose else logging.INFO, stream=sys.stdout)
    exit_code = run(args)
    try:
        os.unlink(_tempfile)
    finally:
        pass
    logging.info("Exiting with status %d (%s)", exit_code, "OK" if exit_code == 0 else "ERROR")
    sys.exit(exit_code)
