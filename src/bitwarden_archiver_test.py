import json
import os
import subprocess
import tempfile

import bitwarden_archiver


def _run(*args):
    args = args or ["--bitwarden-cli", "", "--output-file", ""]
    args = bitwarden_archiver.parser.parse_args(args)
    return bitwarden_archiver.run(args)


def test_non_existing_bw_client_is_an_error():
    non_existing_file = tempfile.mktemp()
    assert _run("--bitwarden-cli", non_existing_file, "--output-file", "") == 1


def test_logged_in_client_is_an_error(mocker):
    mocker.patch("bitwarden_archiver._bw", side_effect=[{"status": "unlocked"}])
    assert _run() == 1


def test_logged_in_client_is_expected_if_use_session_is_specified(mocker):
    mocker.patch("bitwarden_archiver._bw", side_effect=[{"status": "unauthenticated"}])
    assert _run("--use-existing-session", "--output-file", "") == 1


def test_overwrite_with_random_data():
    tmp = bitwarden_archiver._get_temp_file()
    with open(tmp, "wb") as f:
        f.write(b"Hello World!")
    bitwarden_archiver._erase_temp_file()

    with open(tmp, "rb") as f:
        contents = f.read()
    os.unlink(tmp)
    assert len(contents) == 0


def test_read_backup_password(mocker):
    mocker.patch("getpass.getpass", side_effect=["test1", "test1"])
    mocker.patch("builtins.input", side_effect=["y"])
    assert bitwarden_archiver._read_backup_password() == "test1"


def test_read_backup_password_retries_if_two_passwords_dont_match(mocker):
    mocker.patch("getpass.getpass", side_effect=["test1", "test2", "test2", "test2"])
    mocker.patch("builtins.input", side_effect=["y"])
    assert bitwarden_archiver._read_backup_password() == "test2"


def test_read_backup_password_warns_about_weak_password(mocker):
    mocker.patch(
        "getpass.getpass",
        side_effect=["test1", "test1", "o4P3PM*v9vjZE9An@bh7", "o4P3PM*v9vjZE9An@bh7"],
    )
    mocker.patch("builtins.input", side_effect=["n"])
    assert bitwarden_archiver._read_backup_password() == "o4P3PM*v9vjZE9An@bh7"


def _setup_mocks(mocker, steps):
    _bw_site_effect_items = []
    input_responses = []
    getpass_responses = []

    for kind, return_value in steps:
        if kind == "bw_cli":
            _bw_site_effect_items.append(return_value)
        elif kind == "input":
            input_responses.append(return_value)
        elif kind == "getpass":
            getpass_responses.append(return_value)
        else:
            assert False, (kind, return_value)

    def _bw_side_effects(*args, **kwargs):
        del kwargs
        nonlocal _bw_site_effect_items
        item = _bw_site_effect_items[0]
        _bw_site_effect_items = _bw_site_effect_items[1:]
        if item == "EXPORT":
            args = args[0]
            output_fn = args[args.index("--output") + 1]
            with open(output_fn, "w") as f:
                f.write("{}")
        elif item == "EXCEPTION":
            raise subprocess.CalledProcessError(returncode=1, cmd="")
        else:
            return item

    mocker.patch("bitwarden_archiver._bw", side_effect=_bw_side_effects)
    mocker.patch("builtins.input", side_effect=input_responses)
    mocker.patch("getpass.getpass", side_effect=getpass_responses)


def _setup_testcase(mocker, backup_json, retry_password_entry=False, enter_user=True):
    steps = [("bw_cli", {"status": "unauthenticated"})]

    for user, values in backup_json.items():
        if enter_user:
            steps.append(("input", user))
        steps.append(("getpass", "bw_password"))
        if retry_password_entry:
            steps += [
                ("bw_cli", "EXCEPTION"),
                ("input", user),
                ("getpass", "bw_password"),
            ]
        steps += [
            ("bw_cli", 'export BW_SESSION="test_session"'),
            ("bw_cli", {"userId": user}),
            ("bw_cli", "EXPORT"),
        ]

        orgs = [o for o in values.keys() if o != ""]
        steps.append(("bw_cli", [{"id": o, "type": 0} for o in orgs]))
        for _ in orgs:
            steps.append(("bw_cli", "EXPORT"))
        steps.append(("bw_cli", "logout done"))

    steps += [
        ("input", ""),
        ("getpass", "archive_password"),
        ("getpass", "archive_password"),
    ]

    _setup_mocks(mocker, steps)


def _verify_archive_output(backup_json):
    output = tempfile.mktemp()
    assert _run("--output-file", output) == 0
    archive_contents = bitwarden_archiver._read_archive(output, "archive_password")
    os.remove(output)
    assert len(archive_contents) == 1
    assert json.loads(archive_contents["backup.json"]) == backup_json


def _new_archive_test(mocker, backup_json):
    _setup_testcase(mocker, backup_json)
    _verify_archive_output(backup_json)


def test_archive_one_account(mocker):
    _new_archive_test(mocker, {"user1@example.com": {"": {}}})


def test_archive_two_accounts(mocker):
    _new_archive_test(
        mocker,
        {
            "user1@example.com": {"": {}},
            "user2@example.com": {"": {}},
        },
    )


def test_archive_account_with_organizations(mocker):
    _new_archive_test(
        mocker,
        {
            "user1@example.com": {"": {}, "org1": {}},
        },
    )


def test_archive_logged_in_account(mocker):
    _setup_mocks(
        mocker,
        [
            ("bw_cli", {"status": "unlocked"}),
            ("bw_cli", {"userId": "user1@example.com"}),
            ("bw_cli", "EXPORT"),
            ("bw_cli", []),
            ("getpass", "archive_password"),
            ("getpass", "archive_password"),
        ],
    )

    output = tempfile.mktemp()
    mocker.patch("os.environ", side_effect={})
    assert _run("--output-file", output, "--use-existing-session") == 0
    archive_contents = bitwarden_archiver._read_archive(output, "archive_password")
    os.remove(output)
    assert len(archive_contents) == 1
    assert json.loads(archive_contents["backup.json"]) == {"user1@example.com": {"": {}}}


def test_existing_file_is_backed_up_during_archive(mocker):
    _setup_testcase(mocker, {"user1@example.com": {"": {}}})
    output = tempfile.mktemp()
    _run("--output-file", output)

    _setup_testcase(mocker, {"user1@example.com": {"": {}, "org1": {}}})
    assert _run("--output-file", output) == 0
    archive_contents = bitwarden_archiver._read_archive(output, "archive_password")
    os.remove(output)
    assert len(archive_contents) == 2
    assert json.loads(archive_contents["backup.json"]) == {
        "user1@example.com": {"": {}, "org1": {}}
    }
    del archive_contents["backup.json"]
    backup_name, backup_value = list(archive_contents.items())[0]
    assert backup_name.startswith("backup_backups/")
    assert json.loads(backup_value) == {"user1@example.com": {"": {}}}


def test_login_is_retried_on_error(mocker):
    backup_json = {"user1@example.com": {"": {}}}
    _setup_testcase(mocker, backup_json, retry_password_entry=True)
    _verify_archive_output(backup_json)


def test_cant_use_session_and_users():
    assert _run("--use-existing-session", "--output-file=", "--user=test") == 1


def test_backup_with_emails_from_cli_parameters(mocker):
    output = tempfile.mktemp()
    backup_json = {"user1@example.com": {"": {}}, "user2@example.com": {"": {}}}
    _setup_testcase(mocker, backup_json, enter_user=False)
    assert (
        _run(
            "--bitwarden-cli",
            "",
            "--output-file",
            output,
            "--user=user1@example.com",
            "--user=user2@example.com",
        )
        == 0
    )
    archive_contents = bitwarden_archiver._read_archive(output, "archive_password")
    os.remove(output)
    assert len(archive_contents) == 1
    assert json.loads(archive_contents["backup.json"]) == backup_json
